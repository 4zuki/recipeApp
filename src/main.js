import React from 'react';
import ReactDOM from 'react-dom';

import Description from './components/description';
import RecipeDetail from './components/recipe_detail';
import RecipeList from './components/recipe_list_item';
import SearchBar from './components/search_bar';
import API_KEY from './config';

class RecipePage extends React.Component {
  constructor(props) {
    super(props);
    
    //started off as an empty array
    this.state = {
      recipes: [],
      selectedRecipe: null
    };
    
    //recipe array filled with initial search term
    this.recipeSearch('mac and cheese');
  }
  
  recipeSearch(term){
    //
  }
  
  render() {
    return (
      <div>
        <SearchBar onSearchTermChange={recipeSearch} />
        <Description />
        <RecipeDetail recipe={this.state.selectedRecipe} />
        <RecipeList 
          onRecipeSelect={selectedRecipe => this.setState({selectedRecipe}) }
          recipeItems={this.state.recipeItems} />
      </div>
    );
  }
}

ReactDOM.render(<RecipePage />, document.getElementById('app'));
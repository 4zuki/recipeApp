import React from 'react';
import RecipeListItem from './recipe_list_item';

//need to check key and {recipe}
const RecipeList = (props) => {
  const recipeItems = props.recipes.map((recipe) => {
    return (
      <RecipeListItem
        onRecipeSelect={props.onRecipeSelect}
        key={}
        recipe={recipe} />
    );
  });
  
  return (
      <ul className="recipe-results">
        The recipe results that were sampled: 
        {recipeItems}
      </ul>
  );
};

export default RecipeList;
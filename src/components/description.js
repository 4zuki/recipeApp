import React from 'react';

export class Description extends React.Component {
  render() {
    return (
      <div className="description">
        <h2>A Recipe Averager</h2>
        <p>Using the <a href="http://food2fork.com/about/api">Food2Fork API</a>, 
          I created a recipe lookup tool that will take 3 similar recipes and 
          create a new recipe with those averaged ingredients.  </p>
        <p>For example, look up a pancake recipe and you will get different 
          amounts of flour in 3 different recipes that will then become averaged.
        </p>
        <p>To read more about how I made this, check it out on my <a href="https://gitlab.com/4zuki/recipeApp">Gitlab Page</a>.</p>
      </div>
    );
  }
}
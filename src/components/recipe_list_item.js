import React from 'react';

//need to change {recipe.snippet.title}
const RecipeListItem = ({recipe, onRecipeSelect}) => {
  //might need a different path
  const imageUrl = recipe.snippet.thumbnails.default.url;
  
  return (
    <li onClick={() => onRecipeSelect(recipe)} className="list-group-item">
      <div className="recipe-list">
        <div className="list-item-left">
          <img className="list-item-image" src={imageUrl} />
        </div>
        <div className="list-item-body">
          <div className="list-item-heading">
            {recipe.snippet.title}
          </div>
        </div>
      </div>
    </li>
  );
};

export default RecipeListItem;
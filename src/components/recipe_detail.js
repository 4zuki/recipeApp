import React from 'react';

class RecipeDetail extends React.Component {
	render () {
		return (
		<div className="recipe-detail">
          <div className="recipe-name">
			<h1>Recipe Name</h1>
            <span>Source: www.bone.voyage</span>
          </div>
          <div className="recipe-image">
			Image
          </div>
          <div className="ingredients">
			<ul>
              <h2>Ingredients:</h2>
              <li>Eggplant</li>
              <li>Carrots</li>
              <li>Zucchini</li>
              <li>Eggplant</li>
              <li>Carrots</li>
              <li>Zucchini</li>
              <li>Eggplant</li>
              <li>Carrots</li>
            </ul>
          </div>
          <div className="instructions">
			<ol>
              <h2>Instructions:</h2>
              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lobortis feugiat vivamus at augue eget arcu. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus.</li>
              <li>Aliquam ultrices sagittis orci a scelerisque purus semper. Turpis in eu mi bibendum neque egestas congue. Auctor elit sed vulputate mi sit. Eu non diam phasellus vestibulum lorem. Neque convallis a cras semper auctor neque vitae. Tincidunt praesent semper feugiat nibh sed pulvinar proin. Volutpat ac tincidunt vitae semper quis lectus nulla at. Odio aenean sed adipiscing diam.</li>
              <li>Nulla pellentesque dignissim enim sit amet venenatis urna cursus. Purus viverra accumsan in nisl nisi scelerisque. Ultrices sagittis orci a scelerisque purus semper eget. A iaculis at erat pellentesque adipiscing commodo elit at. Sed lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt. Elit duis tristique sollicitudin nibh sit amet commodo nulla.</li>
            </ol>
          </div>
		</div>
		);
	}
};

export default RecipeDetail;